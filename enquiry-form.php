<?php

/**
 * Enquiry Form
 *
 * Plugin Name:	    Enquiry Form
 * Description:	    This plugin is for taking user enquiries from front-end.
 * Version:	    1.0
 * Author:	    Usman
 * Tested up to:    4.9.1
 */

if ( !defined( 'ABSPATH' ) )
    exit;

// Abort loading if WordPress is upgrading
if ( defined( 'WP_INSTALLING' ) && WP_INSTALLING )
    return;

/* * *************************** */
include_once( plugin_dir_path( __FILE__ ) . 'models/model.class.php');
include_once( plugin_dir_path( __FILE__ ) . 'includes/helper.php');
include_once( plugin_dir_path( __FILE__ ) . 'includes/enquiry-form.class.php');
include_once( plugin_dir_path( __FILE__ ) . 'includes/init.class.php');

// Register `init` Actions
add_action( 'init', 'start_form_session', 10 );
add_action( 'init', array( 'EnquiryFormInit', 'register_short_codes' ) );
add_action( 'init', array( 'EnquiryFormInit', 'register_enquiry_form_cpt' ) );

// Register `wp` Actions
add_action( 'wp_login', 'end_form_session' );
add_action( 'wp_logout', 'end_form_session' );

// Register `enqueuing` Actions
add_action( 'wp_enqueue_scripts', array( 'EnquiryFormInit', 'register_enquiry_form_public_assets' ) );

// Register `post` Actions
add_action( 'admin_post_enquiry_form_hook', array( 'EnquiryForm', "validate_enquiry_form" ), 10 ); // for logged in users
add_action( 'admin_post_nopriv_enquiry_form_hook', array( 'EnquiryForm', 'validate_enquiry_form' ), 10 ); // for non-logged in users


/******************************/
