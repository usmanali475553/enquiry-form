<?php

/**
 * @author: Usman 
 */
class EnquiryFormTest extends WP_UnitTestCase
{

    private $class_instance;

    public function setUp()
    {
	parent::setUp();
	$this->class_instance = new EnquiryForm();
    }

    function test_form_validation()
    {
	// Replace this with some actual testing code.
	$_POST = array( 'name' => 'test', 'gender' => 'Male', 'phone' => '', 'email' => '', 'address' => '', 'nationality' => '', 'dob' => '', 'education' => '', 'prefered_mode' => '' );
	$this->assertFalse( $this->class_instance->senetize_post_data() );

	$_POST = array( 'name' => 'test', 'gender' => 'Male', 'phone' => '123', 'email' => 'test@test.com', 'address' => 'Test address', 'nationality' => 'Pakistan', 'dob' => '1-2-1990', 'education' => 'BSCS', 'prefered_mode' => 'email' );
	$this->assertTrue( $this->class_instance->senetize_post_data() );
    }

    function test_save_enquiry_form()
    {
	$_POST = array( 'name' => 'test', 'gender' => 'Male', 'phone' => '123', 'email' => 'test@test.com', 'address' => 'Test address', 'nationality' => 'Pakistan', 'dob' => '1-2-1990', 'education' => 'BSCS', 'prefered_mode' => 'email' );
	$post_id = $this->class_instance->save_enquiry_form();
	$inserted_data = get_post_meta( $post_id );
	foreach ( $inserted_data as $key => $value )
	{
	    $this->assertEquals( $value[ 0 ], $_POST[ $key ] );
	}
    }

}
