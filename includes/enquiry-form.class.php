<?php

/**
 * @author: Usman
 */
class EnquiryForm
{

    private static $instance;

    public static function instance()
    {
	if ( !self::$instance instanceof self )
	{
	    self::$instance = new self;
	}
	return self::$instance;
    }

    public function enquiry_form()
    {
	$enquire_form = '<h1>Post Clients</h1>
		<form class="uk-form-stacked uk-form uk-width-1-1 uk-grid" action="' . admin_url( 'admin-post.php' ) . '" method="POST" name="enquiry-form" id="enquiry-form">'
		. wp_nonce_field( "save-enquiry-form", "enquiry_form" ) . '
                    <input type="hidden" name="action" value="enquiry_form_hook"/>
                    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<label class="uk-form-label">Name</label>
			<input class="uk-width-1-1 ' . get_form_data( 'error', 'name' ) . '" type="text" name="name" palceholder="Name" value="' . get_form_data( 'post', 'name' ) . '"/>
		    </div>
		    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<label class="uk-form-label">Gender</label>
			<select class="uk-width-1-1 ' . get_form_data( 'error', 'gender' ) . '" name="gender">
                            <option value="">Select Gender</option>
			    <option value="male" ' . get_select_form_data( 'post', 'gender', 'male' ) . '>Male</option>
			    <option value="female" ' . get_select_form_data( 'post', 'gender', 'female' ) . '>Female</option>
			    <option value="other" ' . get_select_form_data( 'post', 'gender', 'other' ) . '>Other</option>
			</select>
		    </div>
		    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<label class="uk-form-label">Phone</label>
			<input id="phone" class="uk-width-1-1 ' . get_form_data( 'error', 'phone' ) . '" type="number" name="phone" value="' . get_form_data( 'post', 'phone' ) . '"/>
		    </div>
		    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<label class="uk-form-label">Email</label>
			<input class="uk-width-1-1 ' . get_form_data( 'error', 'email' ) . '" type="email" name="email" value="' . get_form_data( 'post', 'email' ) . '" placeholder="example@example.com" />
		    </div>
		    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<label class="uk-form-label">Nationality</label>
			<input class="uk-width-1-1 ' . get_form_data( 'error', 'nationality' ) . '"type="text" name="nationality" value="' . get_form_data( 'post', 'nationality' ) . '" placeholder="Pakistan"/>
		    </div>
		    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<label class="uk-form-label">Date of birth</label>
			<input class="uk-width-1-1 ' . get_form_data( 'error', 'dob' ) . '" type="date" name="dob" max="' . date( 'Y-m-d' ) . '" value="' . get_form_data( 'post', 'dob' ) . '" placeholder="7 July 1901"/>
		    </div>
		    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<label class="uk-form-label">Education background</label>
			<input class="uk-width-1-1 ' . get_form_data( 'error', 'education' ) . '" type="text" name="education" value="' . get_form_data( 'post', 'education' ) . '" placeholder="Graducation in Computer Sciences" />
		    </div>
		    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<label class="uk-form-label">Preferred mode of contact</label>
			<select class="uk-width-1-1 ' . get_form_data( 'error', 'prefered_mode' ) . '" name="prefered_mode" value="' . get_form_data( 'post', 'prefered_mode' ) . '">
                            <option value="">Select Mode</option>
			    <option value="phone" ' . get_select_form_data( 'post', 'prefered_mode', 'phone' ) . '>Phone</option>
			    <option value="email" ' . get_select_form_data( 'post', 'prefered_mode', 'email' ) . '>Email</option>
			    <option value="none" ' . get_select_form_data( 'post', 'prefered_mode', 'none' ) . '>None</option>
			</select>
		    </div>
                    <div class="valid-feilds uk-width-1-1 uk-margin-bottom">
			<label class="uk-form-label">Address</label>
			<textarea class="uk-width-1-1 ' . get_form_data( 'error', 'address' ) . '" name="address" placeholder="St.# 1234">' . get_form_data( 'post', 'address' ) . '</textarea>
		    </div>
		    <div class="valid-feilds uk-width-1-2 uk-margin-bottom">
			<button type="submit" class="uk-button">Save</button>
		    </div>
		</form>
	';

	reset_form_data( array( 'post', 'error' ) );
	return $enquire_form;
    }

    public function list_enquiries()
    {
	$form_list = new WP_Query( array( 'post_type' => 'enquiryform' ) );
	$data = "<div class='enquiry_list'>";
	if ( $form_list->have_posts() )
	{
	    $data.="<ul class='listing-head'><li>Name</li> <li>Gender</li> <li>Phone</li> <li>Email</li> <li>Address</li> <li>Nationality</li> <li>Date Of Birth</li> <li>Education background</li> <li>Preferred mode of contact</li></ul>";
	    while ( $form_list->have_posts() )
	    {
		$form_list->the_post();
		$post_id = get_the_ID();
		$data.="<ul>";
		$data.="<li>" . get_post_meta( $post_id, "name", true ) . "</li>";
		$data.="<li>" . get_post_meta( $post_id, "gender", true ) . "</li>";
		$data.="<li>" . get_post_meta( $post_id, "phone", true ) . "</li>";
		$data.="<li>" . get_post_meta( $post_id, "email", true ) . "</li>";
		$data.="<li>" . get_post_meta( $post_id, "address", true ) . "</li>";
		$data.="<li>" . get_post_meta( $post_id, "nationality", true ) . "</li>";
		$data.="<li>" . get_post_meta( $post_id, "dob", true ) . "</li>";
		$data.="<li>" . get_post_meta( $post_id, "education", true ) . "</li>";
		$data.="<li>" . get_post_meta( $post_id, "prefered_mode", true ) . "</li>";
		$data.="</ul>";
	    }
	}
	else
	    $data = "No Enqiry Form Post Found";

	$data.="</div>";
	return $data;
    }

    public function validate_enquiry_form()
    {
	if ( isset( $_POST[ 'enquiry_form' ] ) && wp_verify_nonce( $_POST[ 'enquiry_form' ], 'save-enquiry-form' ) )
	{
	    if ( self::instance()->senetize_post_data() )
	    {
		self::instance()->save_enquiry_form();
		wp_redirect( get_site_url() . wp_get_referer() );
	    }
	    else
	    {
		wp_redirect( get_site_url() . wp_get_referer() );
	    }
	}
    }

    public function senetize_post_data()
    {
	$all_ok = true;
	foreach ( $_POST as $key => $value )
	{
	    if ( is_null( trim( $value ) ) || !strlen( trim( $value ) ) )
	    {
		set_form_data( 'error', $key );
		unset_form_data( 'post', $key );
		$all_ok = false;
	    }
	    else
	    {
		set_form_data( 'post', $key, $value );
		unset_form_data( 'error', $key );
	    }
	}

	return $all_ok;
    }

    public function save_enquiry_form()
    {
	$post_id = wp_insert_post( array( 'post_title' => 'Enquiry Form', 'post_type' => 'enquiryform', 'post_content' => 'Data For Enquiry Form', 'post_status' => 'publish' ) );
	foreach ( $_POST as $meta_key => $new_meta_value )
	{
	    unset_form_data( 'post', $meta_key );
	    if ( $meta_key !== '_wp_http_referer' && $meta_key !== 'enquiry_form' && $meta_key !== 'action' )
	    {
		$meta_value = get_post_meta( $post_id, $meta_key, true );
		if ( $new_meta_value && '' == $meta_value )
		    add_post_meta( $post_id, $meta_key, $new_meta_value, true );
		elseif ( $new_meta_value && $new_meta_value != $meta_value )
		    update_post_meta( $post_id, $meta_key, $new_meta_value );
	    }
	}

	return $post_id;
    }

}
