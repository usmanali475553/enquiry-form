<?php

/**
 * @author: Usman 
 */
class EnquiryFormInit
{

    public static function register_short_codes()
    {
	add_shortcode( 'enquiry_form', array( 'EnquiryForm', 'enquiry_form' ) );
	add_shortcode( 'enquiry_list', array( 'EnquiryForm', 'list_enquiries' ) );
    }

    public static function register_enquiry_form_cpt()
    {
	$args = array(
	    'public' => true,
	    'show_in_rest' => true,
	    'label' => 'EnquiryForm'
	);

	register_post_type( 'EnquiryForm', $args );
    }

    public static function register_enquiry_form_public_assets()
    {
	beans_compile_js_fragments( "enquiry-form", array(
	    plugins_url( "enquiry-form/assets/public/enquiry-form.js" ),
	    plugins_url( "enquiry-form/assets/public/libs/intl-tel-input/build/js/intlTelInput.min.js" ),
	    plugins_url( "enquiry-form/assets/public/libs/intl-tel-input/build/js/utils.js" ),
	) );

	beans_compile_css_fragments( "enquiry-form", array(
	    plugins_url( "enquiry-form/assets/public/style.css" ),
	    plugins_url( "enquiry-form/assets/public/libs/intl-tel-input/build/css/intlTelInput.css" ),
	) );
    }

}
