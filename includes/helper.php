<?php

/**
 * @author: Usman 
 */
if ( !function_exists( 'start_form_session' ) )
{

    function start_form_session()
    {
	if ( !session_id() )
	{
	    @session_start();
	}
    }

}


if ( !function_exists( 'end_form_session' ) )
{

    function end_form_session()
    {
	if ( session_id() )
	{
	    session_destroy();
	}
    }

}


if ( !function_exists( 'set_form_data' ) )
{

    function set_form_data( $type, $name, $value = "uk-form-danger" )
    {
	$_SESSION[ $type ][ $name ] = $value;
    }

}


if ( !function_exists( 'get_form_data' ) )
{

    function get_form_data( $type, $name )
    {
	if ( isset( $_SESSION[ $type ][ $name ] ) )
	{
	    return $_SESSION[ $type ][ $name ];
	}
    }

}


if ( !function_exists( 'unset_form_data' ) )
{

    function unset_form_data( $type, $name )
    {
	unset( $_SESSION[ $type ][ $name ] );
    }

}


if ( !function_exists( 'reset_form_data' ) )
{

    function reset_form_data( $data_type )
    {
	foreach ( $data_type as $type )
	{
	    unset( $_SESSION[ $type ] );
	}
    }

}


if ( !function_exists( 'get_select_form_data' ) )
{

    function get_select_form_data( $type, $name, $value )
    {
	if ( isset( $_SESSION[ $type ][ $name ] ) && $_SESSION[ $type ][ $name ] === $value )
	{
	    return "selected='selected'";
	}
    }

}

