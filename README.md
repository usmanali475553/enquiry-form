Enquiry Form
===================

[18.219.122.190](http://18.219.122.190)

### Summary ###

This is very simple plugin to take user enquiries and display them, using custom post type.

### Set up ###

* Simply install the plugin
* Run `bower install bower.json` for downloading any missing front-end dependencies
* Run `composer install` for downloading **PHPUnit** repository ( this has been used for running unit tests ).
* To execute **PHPUnit** tests run the following command:
    `cd unittests && bin/install-wp-tests.sh wp_test root '' 127.0.0.1 latest && ../vendor/bin/phpunit --bootstrap tests/bootstrap.php tests/test-enquiry-form.php`

### Usage ###

* To add up the **Enquiry Form** on any page you can use a simple short code `[enquiry_form]`
* Similarly to list enquiries on a page use `[enquiry_list]`

### Structure ###

The structure of this plugin is pretty simple.

* `enquiry-form.php` is kind of boilerplate for this plugin. It attaches all the necessary actions to WordPress hooks
* `includes/init.class.php` contains functions which add shortcodes, create custom post type (CPT) and load assets in plugin.
* `includes/enquiry-form.class.php` this one is the main file where all action is happening from displaying form to saving that into the DB.
* `includes/helper.php` as its name suggests, it has all the helper functions which are getting used in out plugin.
* `models/model.class.php` this can be used in future for extending this plugin.
* `assets/enquiry-form.js` it has the front-end validations for enquiry form.
* `assets/style.css` it has custom styling for listing enquiries.
* `assets/libs` it has all the helping libraries.