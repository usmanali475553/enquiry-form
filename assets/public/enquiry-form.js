
jQuery( document ).ready( function ( $ ) {
    $( document )
	    .off( "submit", "#enquiry-form" )
	    .on( "submit", "#enquiry-form", function ( e ) {
		return form_validation( $ );
	    } );

    $( document )
	    .off( "focus", "#enquiry-form input, #enquiry-form textarea, #enquiry-form select" )
	    .on( "focus", "#enquiry-form input, #enquiry-form textarea, #enquiry-form select", function ( ) {
		$( this ).removeClass( "uk-form-danger" );
	    } );

    $( "#phone" ).intlTelInput( {
	separateDialCode: true,
	initialCountry: "auto",
	formatOnDisplay: true,
	geoIpLookup: function ( callback ) {
	    $.get( 'https://ipinfo.io', function ( ) {
	    }, "jsonp" ).always( function ( resp ) {
		var countryCode = ( resp && resp.country ) ? resp.country : "";
		callback( countryCode );
	    } );
	}

    } );

    $( document )
	    .off( "blur", "#phone" )
	    .on( "blur", "#phone", function ( ) {
		phone_validation( $ );
	    } )
	    .off( "keyup change", "#phone" )
	    .on( "keyup change", "#phone", function ( ) {
		$( this ).removeClass( "uk-form-danger" );
	    } );
} );

function validate_email( email ) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test( String( email ).toLowerCase( ) );
}

function form_validation( $ ) {
    var all_ok = true;

    $( "#enquiry-form input[type='text'],#enquiry-form input[type='email'],#enquiry-form input[type='date'],#enquiry-form input[type='number'],#enquiry-form textarea,#enquiry-form select" ).each( function ( e ) {
	if ( $( this ).val( ).trim( ) ) {
	    $( this ).removeClass( "uk-form-danger" );
	} else {
	    $( this ).addClass( "uk-form-danger" );
	    all_ok = false;
	}
    } );

    if ( validate_email( $( "input[type=email]" ).val( ) ) ) {
	$( this ).removeClass( "uk-form-danger" );
    } else {
	$( this ).addClass( "uk-form-danger" );
	all_ok = false;
    }

    all_ok = phone_validation( $ );
    return all_ok;
}

function phone_validation( $ ) {
    $( "#phone" ).removeClass( "uk-form-danger" );
    if ( $.trim( $( "#phone" ).val( ) ) ) {
	if ( $( "#phone" ).intlTelInput( "isValidNumber" ) ) {
	    $( "#phone" ).removeClass( "uk-form-danger" );
	    return true;
	} else {
	    $( "#phone" ).addClass( "uk-form-danger" );
	    return false;
	}
    } else {
	$( "#phone" ).addClass( "uk-form-danger" );
	return false;
    }
}